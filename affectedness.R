
# check if all of the packages are installed
packages <- c("ggplot2", "stringr", "ddalpha", "mrfDepth", "openxlsx", "mclust", "dplyr", "tidyr")
packages.installed <- rownames(installed.packages())
packages.missing   <- setdiff(packages, packages.installed)
if (length(packages.missing) > 0) {
	write("Error: some packages are missing, install them with install.packges():", stderr())
	# write("Error: some packages are missing, install them with install.packges() or using with 'affectedness -dependencies T':", stderr())
	write(packages.missing, stderr())
	q(status = 1)
}

# Don't produce any uncontrolled output
pdf(NULL)

# Function to check arguments
checkArgs <- function(argv, expectArgs) {
	if (length(argv) < 2) return(FALSE)
	argNames <- names(argv)
	hits1 <- sapply(argNames, function(x) {
		hit <- x %in% names(expectArgs)
		if (!hit) write(paste("Error: unexpected argument", x), stderr())
		return(hit)
	})
	if (sum(expectArgs) > 0) {
		hits2 <- sapply(names(expectArgs[expectArgs]), function(x) {
			hit <- x %in% argNames
			if (!hit) write(paste("Error: argument", x, "not specified"), stderr())
			return(hit)
		})
	} else {
		hits2 <- T
	}
	return(prod(hits1) & prod(hits2))
}

# Parse arguments
getOpts <- function(expectArgs, help) {
	names(expectArgs) <- paste0("-", names(expectArgs))
	args <- commandArgs(trailingOnly=T)
	argNames <- args[c(T,F)]
	if (length(args) == 0 | "-h" %in% argNames || "-help" %in% argNames) {
		write(help, stderr())
		return(list())
	}
	if (length(args) %% 2) {
		write("Error: values for some of the arguments are not set", stderr())
		return(list())
	}
	argv <- args[c(F,T)]
	names(argv) <- substr(argNames, 2, nchar(argNames))
	as.list(argv)
}

# Parse the arguments
help <- "Use as follows:
Rscript affectedness.R [arguments]
 -input        - input xls file with measurements
 -sheet        - sheet with the data [first by default]
 -group        - group column ['group' by default]
 -control      - control group ['Sham' by default]
 -parameters   - file with parameter names and directionality
 -affectedness - column with affectedness index [optional]
 -ignore       - ignore column
 -output       - output xls file
 -dir          - directory for plots
 -adjust       - adjust for parameter distribution skewness (T/F) [F by default]
 -control_unaf - minimal proportion of unaffected animals in the control [0.85]
 -total_interm - proportion of intermediate animals relative to the 95% range of outlyingness [0.1]
 -aff_tolerate - tolerated descrease in proportion of affected animals in the treatments tolerated [0]
 -config       - configuration/defaults file"

expectArgs <- c(input=T, sheet=F, group=F, control=F, parameters=T, affectedness=F, ignore=F, output=T, dir=T, config=F, adjust=F, control_unaf=F, total_interm=F, aff_tolerate=F)
argv <- getOpts(expectArgs, help)

# Check if the arguments are specified in a config file
if (!is.null(argv$config)) {
	config <- new.env()
	source(argv$config, config)
	for (name in names(config)) {
		if (is.null(argv[[name]])) argv[[name]] <- config[[name]]
	}
}
if (!checkArgs(argv, expectArgs)) q(status=1)

# Load all of the packages
invisible(
	sapply(packages, function(package) suppressMessages(library(package, character.only = T)))
)

# Assign default values (when needed)

input   <- argv$input
sheet   <- ifelse(is.null(argv$sheet),          1, argv$sheet)
group   <- ifelse(is.null(argv$group),    'group', argv$group)
control <- ifelse(is.null(argv$control),   'Sham', argv$control)
control_unaf <- ifelse(is.null(argv$control_unaf),   0.85, argv$control_unaf)
total_interm <- ifelse(is.null(argv$total_interm),    0.1, argv$total_interm)
aff_tolerate <- ifelse(is.null(argv$aff_tolerate),      0, argv$aff_tolerate)
parameters <- read.table(argv$parameters, col.names = c("", "side"), sep = "\t", row.names = 1)
extra.sides <- ! parameters$side %in% c("lower","upper","both")
if (sum(extra.sides) > 0) {
	write("Error: the -parameters file contains unsupported sides, the allowed values are 'lower', 'upper' and 'both'", stderr())
	q(status=1)
}

adjust  <- as.logical(ifelse(is.null(argv$adjust), F, argv$adjust))

if (is.na(adjust)) {
	write("Error: the -adjust parameter must be a boolean flag (T/F)", stderr())
	q(status=1)
}
affectedness <- argv$affectedness
ignore       <- argv$ignore
output       <- argv$output

# All columns to consider
allcols <- c(rownames(parameters), affectedness, group, ignore)

# Read the data
coldata <- suppressMessages(read.xlsx(input, sheet, na.strings = c("", "-", "?"), colNames = F, rows = 1))
data <- suppressMessages(read.xlsx(input, sheet, na.strings = c("", "-", "?")))
names(data) <- sub("\\s+$", "", coldata[1,])

# Check that all of the columns are in place
missing <- setdiff(allcols, names(data))
if (length(missing) > 0) {
	write(paste("Error: columns not found:", paste(missing, collapse=", ")), stderr())
	q(status=1)
}

# Control that the "ignore" column is boolean (if needed)
if (!is.null(ignore)) {
	if (!is.logical(data[,ignore]) && !is.numeric(data[,ignore])) {
		write("Error: the ignore column is expected to be boolean", stderr())
		q(status=1)
	}
	is.na(data[,ignore]) <- FALSE
	data <- data[ !data[,ignore], ]
}

# Check that the control is there
data$group <- as.factor(sub("\\s+$", "", data[,group]))
groups <- levels(data$group)
if (! (control %in% groups)) {
	write(paste0("Control '", control, "' is not found in the group column '", group, "'"), stderr())
	q(status=1)
}

data.iscontrol  <- data$group == control
data.parameters <- data[,rownames(parameters)]
non.numeric <- lapply(data.parameters, function(x) !is.numeric(x))
non.numeric.cols <- names(Filter(sum, non.numeric))
if (length(non.numeric.cols) > 0) {
	write("Error: the following columns appear to be non-numeric - the allowed values are numbers or any of '-', '' or '?' for missing data", stderr())
	write(non.numeric.cols, stderr())
	q(status = 1)
}

# Median imputation for missing data
data.parameters <- data.frame(lapply(data.parameters, function(x) ifelse(is.na(x), median(x, na.rm = T), x)))

data$depth <- depth.projection(data.parameters, data.parameters[data.iscontrol,])

# Pick the outlyingness function
outl_fun <- ifelse(as.logical(adjust), adjOutl, outlyingness)

# Iterate over parameters to calculate outlyingness for each one
outl <- lapply(rownames(parameters), function(x) {
	# Get vector of observed flags (TRUE/FALSE)
	observed <- !is.na(data[,x])
	names(observed) <- rownames(data)

	# Get all observed data for this parameter
	x.all <- data[observed, x]
	# Get vector of values for control observations
	x.control <- x.all[data.iscontrol]
	# Get the median
	med.control <- median(x.control)

	# Calculate the adjusted outlyingness
	outlZ <- outl_fun(x.control, x.all, list(ndir = 1))$outlyingnessZ

	# Replace potential zero outlyingness with next-smallest value
	outlSelf <- outl_fun(x.control, x.control, list(ndir = 1))$outlyingnessZ
	minOutl <- min(outlSelf[outlSelf>0])
	outlZ[outlZ < minOutl] <- minOutl

	# Set outlyingness to the minimum if the corresponding parameter
	# should be treated asymmetrically
	side <- parameters[x,"side"]
	if (side == "lower") {
		outlZ[x.all > med.control] <- minOutl
	} else if (side == "upper") {
		outlZ[x.all < med.control] <- minOutl
	}

	# Unfold items with missing data
	names(outlZ) <- rownames(data[observed,])
	df <- merge(observed, outlZ, by = "row.names", all.x = T)
	df$order <- as.numeric(df$Row.names)
	df <- df[order(df$order),]
	return(df$y)
})

# Average outlyingness
names(outl) <- paste0(rownames(parameters), "_outl")
data <- cbind(data, do.call(cbind, outl))
# as geometric mean
data$average_outl <- apply(data[,names(outl)], 1, function(x) exp(mean(log(x[!is.na(x)]))))
# as arithmetic mean
# data$average_outl <- apply(data[,names(outl)], 1, function(x) mean(x[!is.na(x)]))

###########################################################################
# Several different approaches to finding the affected and the unaffected #
###########################################################################

###########################################################################
# Based on z-scores for average_outl with respect to controls             #
# calculated from robust rank-based estimate of standard deviation        #
###########################################################################

# control.outl <- subset(data, group == control)$average_outl
# control.q <- quantile(control.outl, probs = c(0.25, 0.5, 0.75))
# control.est <- list(mean = control.q[2], sd = (control.q[3] - control.q[1]) * 0.7413)
# data$z_score <- (data$average_outl - control.est$mean) / control.est$sd
# data$p_affected_z_score <- ifelse(data$z_score < 0, 0, pnorm(data$z_score) - pnorm(-data$z_score))

###########################################################################
# k-means                                                                 #
###########################################################################

#km <- kmeans(data$average_outl, centers=2, nstart=25)
#data$cluster <- km$cluster

###########################################################################
# Gaussian mixture models                                                 #
###########################################################################

#gauss <- Mclust(data$average_outl, G = 2, modelNames = "E")

#data$affected   <- gauss$classification == 2
#data$p_affected <- gauss$z[,2]

#data$p_unaffected <- 1 - data$p_affected
#data$q_affected   <- p.adjust(data$p_affected,   "fdr")
#data$q_unaffected <- p.adjust(data$p_unaffected, "fdr")

############################################################################

############################################################################
# Define the affected group based on fixed thresholds                      #
############################################################################

outl.quantile  <- data %>% pull(average_outl) %>% quantile(probs = 0.95)
assign.class <- function(data, unaffected.thr, intermediate.thr) {
	mutate(data, class = ifelse(average_outl < unaffected.thr, "unaffected", ifelse(average_outl < intermediate.thr, "intermediate", "affected")))
}

# Calculate the thresholds for unaffected and intermediates
data.sorted <- data %>% arrange(average_outl)
control.num <- data %>% filter(group == control) %>% nrow
unaffected.thr <- data.sorted %>% filter(group == control) %>% tail(n = -round(control.num * control_unaf)) %>% head(n = 1) %>% pull(average_outl)
intermediate.thr <- unaffected.thr + outl.quantile * total_interm
data.sorted <- data.sorted %>% assign.class(unaffected.thr, intermediate.thr)

# Re-adjust the thresholds by shifting them to the right if possible and
# tolerating up to a aff_tolerate% reduction in the number of affected among treated
affected.among.treated <- data.sorted %>% filter(class == "affected", group != control) %>% nrow
intermediate.thr <- data.sorted %>% filter(group != control) %>% tail(n = round(affected.among.treated * (1 - aff_tolerate))) %>% head(n = 1) %>% pull(average_outl)
unaffected.thr <- intermediate.thr - outl.quantile * total_interm
data <- data %>% assign.class(unaffected.thr, intermediate.thr)

# Test for differences in average outlyingness between groups
con <- file(paste0(output, ".test.txt"))
sink(con)
if (length(groups) > 2) {
	library(dunn.test)
	my.test <- dunn.test(data$average_outl, data$group, kw = T, method = "bh", altp = T)
} else {
	my.test <- wilcox.test(data$average_outl ~ data$group)
	print(my.test)
}
sink()
close(con)

# Write the output data
write.xlsx(data, output)

# Labels for plots
my.labels <- list(average_outl = "Average outlyingness", depth = "Depth", affectedness_index = "Index of affectedness")

# Create the directory for the plots
dir.create(argv$dir, showWarnings = F)

# Function to plot xy dotplots for two parameters
plot_corr <- function(param1, param2, show.corr = T) {
	data.plot <- data[,c(param1, param2, group)]
	names(data.plot) <- c("param1", "param2", "group")
	data.plot <- subset(data.plot, !is.na(param1) & !is.na(param2))
	lab1 <- ifelse(is.null(my.labels[[param1]]), param1, my.labels[[param1]])
	lab2 <- ifelse(is.null(my.labels[[param2]]), param2, my.labels[[param2]])
	g <- ggplot(data.plot, aes(x = param1, y = param2, fill = group)) +
		scale_fill_manual(values = c("#bb4340","#81b87d","yellow","orange","cyan")) +
		geom_point(shape = 21, size = 5) +
		xlab(lab1) + ylab(lab2)
	if (show.corr) {
		corr <- cor(data.plot$param1, data.plot$param2)
		g <- g + annotate("text", label = paste("r =", round(corr, digits=2)), x = Inf, y = Inf, size = 8, colour = "red", vjust = "inward", hjust = "inward")
	}
	ggsave(paste0(argv$dir, "/", param1, "-", gsub("/", "", param2), ".pdf"), width = 7, height = 7, g)
}

# Plot every parameter against average outlyingness
invisible(sapply(rownames(parameters), function(param) {
	plot_corr(param, "average_outl", show.corr = F)
}))

# Plot distribution of average outlyingess colored by k-mean cluster assignment
# (experimental)
#g <- ggplot(data, aes(x = average_outl, fill = as.factor(cluster))) +
#	geom_histogram(bins = 30) +
#	xlab(my.labels$average_outl) + ylab("Number of individuals") + facet_wrap(. ~ group, ncol = 1) + theme_bw()
#ggsave(paste0(argv$dir, "/average_outl_kmeans.pdf"), g, width = 7, height = 7)


# A function to add transparency to the average outlyingness distribution
#match_outl <- function(xmin, xmax) with(data, mean(p_affected[average_outl > xmin & average_outl <= xmax]))
# Plot distribution of average outlyingess gradient-colored by p-affected
#g <- ggplot(data, aes(x = average_outl)) +
#	geom_histogram(binwidth = 0.1, aes(fill = group, alpha = mapply(match_outl, ..xmin.., ..xmax..))) +
#	labs(alpha = "Pr(affected)") + labs(fill = "Group") + xlab(my.labels$average_outl) + ylab("Number of individuals") + facet_wrap(. ~ group, ncol = 1) +
#	scale_fill_manual(values = c("#5d6c53", "#d35f5f", "green", "blue", "yellow")) +
#	scale_alpha(breaks = seq(0.1, 1, 0.2), range = c(0.25, 1)) +
#	theme_bw() +
#	theme(strip.background = element_blank(), strip.text.x = element_blank())
#ggsave(paste0(argv$dir, "/average_outl.pdf"), g, width = 7, height = 7)

# Plot distribution of average outlyingess
g <- ggplot(data, aes(x = average_outl)) +
	geom_histogram(binwidth = 0.1, aes(fill = group)) +
	geom_vline(xintercept = unaffected.thr, linetype = "dotted", color = "blue") + geom_vline(xintercept = intermediate.thr, linetype = "dotted", color = "red") +
	labs(fill = "Group") + xlab(my.labels$average_outl) + ylab("Number of individuals") + facet_wrap(. ~ group, ncol = 1) +
	scale_fill_manual(values = c("#5d6c53", "#d35f5f", "green", "blue", "yellow")) +
	theme_bw() +
	theme(strip.background = element_blank(), strip.text.x = element_blank())
ggsave(paste0(argv$dir, "/average_outl.pdf"), g, width = 7, height = 7)

# Analogous plots for depth, although we don't really use them
#match_depth <- function(xmin, xmax) with(data, mean(p_affected[depth > xmin & depth <= xmax]))
#g <- ggplot(data, aes_string(x = "depth")) + geom_histogram(bins = 30, aes(fill = mapply(match_depth, ..xmin.., ..xmax..))) + labs(fill = "Pr(affected)") + xlab(my.labels$depth) + ylab("Number of individuals") + facet_wrap(. ~ group, ncol = 1) + theme_bw()
#ggsave(paste0(argv$dir, "/depth.pdf"), g, width = 7, height = 7)

# Plot average outlyingness vs. depth
plot_corr("average_outl", "depth")

# Plot affectedness index vs. average outlyingness and depth
if (!is.null(affectedness)) {
	plot_corr("average_outl", affectedness)
	plot_corr("depth", affectedness)
}

# Count affected, unaffected and ucertain specimens
affected_counts <- data %>% group_by(group, class) %>% summarize(n = n()) %>% spread(key = class, value = n)
write.xlsx(affected_counts, paste0(argv$dir, "/affected_counts.xlsx"))
